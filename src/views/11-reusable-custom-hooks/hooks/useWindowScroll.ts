import { useWindowEvent } from './useWindowEvent';

export const useWindowScroll = (cb: (e: Event) => void) => useWindowEvent('scroll', cb);
