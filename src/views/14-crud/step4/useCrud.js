import { useReducer, useEffect } from 'react'

const initialState = {
  initLoading: true,
  loading: false,
  error: null,
  data: null
}

const headers = {
  "Content-type": "application/json; charset=UTF-8"
}

export default function useCrud (initUrl, reducer) {
  const [state, dispatch] = useReducer(reducer, initialState)

  // init loading
  useEffect(() => {
    dispatch({ type: 'init_start' })
    fetch(initUrl)
      .then(res => res.json())
      .then(data => dispatch({ type: 'init_success', data }) )
      .catch(err => dispatch({ type: 'init_failure', error: 'Failed to load init product.' }))
  }, [initUrl])

  // crud operations
  const addItem = (url, item) => {
    dispatch({ type: 'start' })
    return fetch(url, { method: 'POST', body: JSON.stringify(item), headers })
      .then(res => res.json())
      .then(data => dispatch({ type: 'add_success', data }))
      .catch(err => dispatch({ type: 'add_failure', error: 'Failed to add.' }))
  }

  const deleteItem = (url, item) => {
    dispatch({ type: 'start' })
    return fetch(url, { method: 'DELETE' })
      .then(res => res.json())
      .then(() => dispatch({ type: 'delete_success', data: item }))
      .catch(err => dispatch({ type: 'delete_failure', error: 'Failed to delete.' }))
  }

  const crudOps = { addItem, deleteItem }
  return [state, crudOps]
}
