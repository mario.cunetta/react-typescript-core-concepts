import React from "react"

export const DemoChildren: React.FC = () => {
  return (
    <div className="example-box-centered small">
      <Panel title="hello form">
        <input type="text"/>
        <input type="text"/>
      </Panel>

      <hr/>

      <Panel>
        <button>Go</button>
      </Panel>
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH CHILDREN
// TS PropTypes: https://github.com/typescript-cheatsheets/react-typescript-cheatsheet#basic-prop-types-examples
// =========================================

interface PanelProps {
  title?: string;
  children?: React.ReactNode;
}

export const Panel: React.FC<PanelProps> = ({title, children}) => {
  return (
    <div className="card">
      { title && <div className="card-header">{title}</div> }
      <div className="card-body">{children}</div>
    </div>
  )
};
