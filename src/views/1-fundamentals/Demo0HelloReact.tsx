import React from "react"

export const DemoHelloReact: React.FC = () => {
  const yourname = 'Fabio';

  return (
    <h1 className="example-box-centered">
      Hello {yourname}
    </h1>
  )
};


