import React, { useEffect, useState } from 'react'

export const Demo8simpleHooks: React.FC = () => {
  let [count, setCount] = useState<number>(0);

  useEffect(() => {
    const id = setInterval(() => {
      setCount(++count);
    }, 1000);

    return () => {
      clearInterval(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function openURL(url: string) {
    window.open(url);
  }

  return (
    <div className="example-box-centered small">
      <Panel
        title={'counter: ' + count}
        icon="fa fa-link"
        onIconClick={() => openURL('http://www.google.com')}
      > Click Icon on header </Panel>
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH ICON CALLBACK
// =========================================

interface PanelProps {
  title ? : string;
  children?: React.ReactNode;
  icon?: string;
  onIconClick: () => void;
}

export const Panel: React.FC<PanelProps> = ({
  icon, title, onIconClick, children
}) => {

  const iconCls = `${icon} pull-right cursor`;

  return (
    <div className="card">
      <div className="card-header">
        { title }
        { icon && <i className={iconCls} onClick={onIconClick} />}
      </div>
      <div className="card-body">{children}</div>
    </div>
  )
};
