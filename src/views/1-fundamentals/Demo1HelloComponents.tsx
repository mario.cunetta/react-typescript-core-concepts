import React from "react"

export const DemoHelloComponents: React.FC = () => {
  return (
    <div className="example-box-centered">
      <Panel title="hello react" text="How are you?" />
      <Panel title="hello Angular" text={(
        <input type="text" defaultValue="write something... do nothing"/>
      )} />
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT
// =========================================

interface PanelProps {
  title?: string;
  text?: React.ReactNode;
}

export const Panel: React.FC<PanelProps> = props => {
  return (
    <div className="card">
      <div className="card-header">{props.title}</div>
      <div className="card-body">{props.text}</div>
    </div>
  )
};
