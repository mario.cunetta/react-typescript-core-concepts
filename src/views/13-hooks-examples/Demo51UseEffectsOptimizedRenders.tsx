import React, { useEffect, useState } from 'react';

// GOAL:
// use useEffects with props to avoid multiple effect initializations
export const DemoUseEffectOptimized: React.FC = () => {

  const [count, setCount] = useState(0)

  return (
    <>
      <button onClick={() => setCount(count =>  count + 1)}>+ { count }</button>
      <Example title="CIAO A TUTTI" count={count}></Example>
    </>
  );
}

interface DemoProps {
  count: number;
  title: string;
}
export const Example: React.FC<DemoProps> = props => {
  const [countMultiplied, setCount] = useState(0)
  const [title, setTitle] = useState('');

  useEffect(() => {
    setCount(props.count * 10)
  }, [ props.count])

  useEffect(() => {
    setTitle(props.title.toLowerCase())
  }, [ props.title])

  return (
    <>
      <h4>{ title }</h4>
      counter --> { countMultiplied }
    </>
  );
}
