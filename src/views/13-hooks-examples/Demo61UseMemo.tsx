import React, { useEffect, useState } from 'react'

export const DemoUseMemo: React.FC = () => {
  let [value, setValue] = useState('hello')

  useEffect(() => {
    const id = setInterval(() => {
      setValue('another')
    }, 1000);

    return () => {
      clearInterval(id);
    }
  }, []);

  return <div>
    THIS EXAMPLE IS NOT A VALID EXAMPLE OF useMemo. TO FIX
    <Panel1 value={value} title="without useMemo" />
    <Panel2 value={value} title="with useMemo"/>
  </div>
}

interface PanelProps {
  value: string;
  title: string
}
export const Panel1: React.FC<PanelProps> = ({value, title}) => {
  console.log('render')
  return (
    <div>
      {title}
      <h1>{value}</h1>
    </div>
  );
}
export const Panel2 = React.memo(Panel1);
